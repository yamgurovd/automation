import requests

# Пример 1 GET запрос для проверки статус кода 301 для работы с заголовками
headers = {
    "some_headers": "123"
}

url1 = 'https://playground.learnqa.ru/api/show_all_headers'
res1 = requests.get(url=url1, headers=headers)

print(res1.json())
print(res1.headers)