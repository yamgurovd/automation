'''Ex7: Запросы и методы

Сегодня задача должна быть попроще. У нас есть вот такой URL: https://playground.learnqa.ru/ajax/api/compare_query_type
Запрашивать его можно четырьмя разными HTTP-методами: POST, GET, PUT, DELETE

При этом в запросе должен быть параметр method. Он должен содержать указание метода, с помощью которого вы делаете запрос.
Например, если вы делаете GET-запрос, параметр method должен равняться строке ‘GET’. Если POST-запросом -
то параметр method должен равняться ‘POST’. И так далее.

Надо написать скрипт, который делает следующее:

1. Делает http-запрос любого типа без параметра method, описать что будет выводиться в этом случае.
2. Делает http-запрос не из списка. Например, HEAD. Описать что будет выводиться в этом случае.
3. Делает запрос с правильным значением method. Описать что будет выводиться в этом случае.
4. С помощью цикла проверяет все возможные сочетания реальных типов запроса и значений параметра method.
Например с GET-запросом передает значения параметра method равное ‘GET’, затем ‘POST’, ‘PUT’, ‘DELETE’ и так далее.
И так для всех типов запроса. Найти такое сочетание, когда реальный тип запроса не совпадает со значением параметра,
но сервер отвечает так, словно все ок. Или же наоборот, когда типы совпадают, но сервер считает, что это не так.

Не забывайте, что для GET-запроса данные надо передавать через params=
А для всех остальных через data=

Итогом должна быть ссылка на коммит со скриптом и ответы на все 4 вопроса.
'''
import requests

url = 'https://playground.learnqa.ru/ajax/api/compare_query_type'

# 1 пункт
responce1 = requests.get(url)
print(f"Ответ от сервера, без параметра 'method' в запросе: {responce1.text}, статус код: {responce1.status_code}")

# 2 пункт
responce2 = requests.head(url)
print(f"Ответ от сервера не из списка метод HEAD в запросе: {responce2.text}, статус код: {responce2.status_code}")

# 3 пункт
payload = {
    "method": "GET"
}
responce2 = requests.get(url, params=payload)
print(f"Ответ от сервера, c параметром 'method' в запросе GET: {responce2.text}, статус код: {responce2.status_code}")

# 4 пункт
meth_list = ['GET', 'POST', 'PUT', 'DELETE', 'test']

for meth_item in meth_list:
    if meth_item == "GET":
        responce = requests.get(url, params={"method": meth_item})
        print(f"Ответ сервера: {responce.text}, статус код {responce.status_code}")

    elif meth_item == "POST":
        responce = requests.post(url, data={"method": meth_item})
        print(f"Ответ сервера: {responce.text}, статус код {responce.status_code}")

    elif meth_item == "PUT":
        responce = requests.put(url, data={"method": meth_item})
        print(f"Ответ сервера: {responce.text}, статус код {responce.status_code}")

    elif meth_item == "DELETE":
        responce = requests.delete(url, data={"method": meth_item})
        print(f"Ответ сервера: {responce.text}, статус код {responce.status_code}")

    else:
        print(f'Параметр "method" не имеет значение {meth_item} !!!')
