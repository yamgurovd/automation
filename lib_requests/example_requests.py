from json.decoder import JSONDecodeError
import requests

# Пример 1 GET без параметра
url1 = 'https://playground.learnqa.ru/api/get_text'
res1 = requests.get(url1)

try:
    parsed_responce_text = res1.json()
    print(parsed_responce_text)
except JSONDecodeError:
    print('Responce is not JSON format')

# Пример 2 GET с параметром и в JSON формате
url2 = 'https://playground.learnqa.ru/api/hello'
payload2 = {"name": "Dayan"}

res2 = requests.get(url2, params=payload2)
temp = res2.json()  # Временная переменная

print(temp.get('answer', 'Такого ключа нет'))

# Пример 3 GET с параметром не в JSON формате
url3 = 'https://playground.learnqa.ru/api/check_type'
params = {"param1": "value1",
          "param2": "value2"
          }
res3 = requests.get(url3, params=params)

print(res3.text)

# Пример 4 POST с параметром не в JSON формате
url4 = 'https://playground.learnqa.ru/api/check_type'
payload4 = {"param1": "value1",
            "param2": "value2"
            }
res3 = requests.post(url3, data=params)

print(res3.text)
