import requests

# Пример 1 POST запрос для проверки статус кода 200
url1 = 'https://playground.learnqa.ru/api/check_type'
res1 = requests.post(url1)

print(res1.status_code)

# Пример 2 POST запрос для проверки статус кода 500
url2 = 'https://playground.learnqa.ru/api/get_500'
res2 = requests.post(url2)

print(res2.status_code)

# Пример 3 POST запрос для проверки статус кода 301(redirect)
url3 = 'https://playground.learnqa.ru/api/get_301'
res3 = requests.post(url3, allow_redirects=True)

print(res3.status_code)

first_res = res3.history[0]
second_res = res3

print(first_res.url)
print(second_res.url)
