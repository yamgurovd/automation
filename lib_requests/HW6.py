'''Ex6: Длинный редирект
Необходимо написать скрипт, который создает GET-запрос на метод: https://playground.learnqa.ru/api/long_redirect

С помощью конструкции response.history необходимо узнать, сколько редиректов происходит от изначальной
точки назначения до итоговой. И какой URL итоговый.

Ответ опубликуйте в виде ссылки на коммит со скриптом, а также укажите количество редиректов и конечный URL.
'''
import requests

url = 'https://playground.learnqa.ru/api/long_redirect'
responce = requests.get(url=url)

count = 0
redirects = responce.history

for redirect_item in redirects:
    count += 1

    if redirect_item == redirects[-1]:
        print(f"Количество redirects: '{count}'")
        print(f"Конечный URl: '{redirect_item.url}'")
