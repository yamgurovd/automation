import requests

# Пример 1 POST запрос для проверки куки
payload = {
    "login": "secret_login",
    "password": "secret_pass"
}
url1 = 'https://playground.learnqa.ru/api/get_auth_cookie'

res1 = requests.post(url=url1, data=payload)

print(res1.text)
print(res1.status_code)
print(dict(res1.cookies))  # Куки нужно привести к словарю, чтобы увидеть его в отформатированном виде

cookie_value = res1.cookies.values()  # Можно получать значения куки 2 способами 1 способ мы получаем ввиде списка
cookie_value1 = res1.cookies.get('auth_cookie')

cookies = {}
if cookies is not None:
    cookies.update({
        "auth_cookie": cookie_value1
    })

print(cookie_value)
print(cookie_value1)

# Создаем второй запрос для отправки полученное куки
url2 = 'https://playground.learnqa.ru/api/check_auth_cookie'

res2 = requests.post(url=url2, cookies=cookies)

print(res2.text)
